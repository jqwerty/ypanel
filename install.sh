#!/bin/bash
echo "Installing...."

#updating all dependencies
sudo apt update

#installing apache2
sudo apt purge apache2
sudo apt install apache2
sudo systemctl enable apache2

#configure apache2
sudo ufw allow 80/tcp

#installing MySQL
sudo apt purge mysql-server
sudo apt install mysql-server
sudo systemctl enable mysql